
const express = require('express')
//para mandar requests a 4ch
const axios = require('axios');
//para quitar palabras rollo 'what' 'the' 'by'...
const stopword = require('stopword');
const json2html = require('node-json2html')
const app = express()
const port = 4000



const terms = {};

app.get('/', (req, res) => {
    var termsNew = [{}];
    for (term of Object.keys(terms)) {
        termsNew.push({title: term, times: terms[term].times});
    }

    //ANTENSHON
    //esto es para ke kede mas bonito cuando te metes en la ruta '/' desde el navegador
    //cuando vayas a montar el frontend envezde res.send(html) devuelve el 'termsNew'
    let template = {'<>':'div','html':'${title} ${times}'};
    
  
    let html = json2html.transform(termsNew, template);
    res.send(html);
  
  })

function downloadData() {
    axios.get('https://a.4cdn.org/g/threads.json').then(function(res) {
        const data = res.data;

        var threads = data[0].threads;
        for (thread of threads) {
            axios.get(`https://a.4cdn.org/g/thread/${thread.no}.json`).then(function(res) {
                var post = res.data.posts[0];
                analizatzaile(post.com);
            });
        }
    });
}

function analizatzaile(body) {
    //regex, aprendetelo
    let re = /\.|,|:|\?|!/g;

    var body2 = body.replace(re, '')
    var cleaned = stopword.removeStopwords(body2.split(' '));
    for(word of cleaned) {
        if (word.length > 3 && word.length < 12) {
            saveTerm(word);
        }
    }
}

function saveTerm(term) {
    if(terms[term]) {
        terms[term].times += 1;
        return;
    }
    terms[term] = {times: 1};
    console.log(terms);


}

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
  downloadData();
})
